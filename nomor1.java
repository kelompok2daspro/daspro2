package soal;

import java.util.Scanner;

public class nomor1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = 15;
        int b = 180;
        int c, d, i, j, k, v, x, y, z;
        System.out.print("Masukkan bilangan pertama : "+a);
        System.out.println("");
        System.out.print("Masukkan bilangan kedua : "+b);
        System.out.println("");
        System.out.print("Faktor persekutuan dari "+a+" adalah ");
        for (i = 1; i <= a; i++) {
            c = a / i;
            for (i = 1; i <= c; i++) {
                if (c % i == 0) {
                    x = i;
                    System.out.print(x + ", ");
                }
            }
        }
        System.out.println("");
        System.out.print("Faktor persekutuan dari " + b + " adalah ");
        for (j = 1; j <= b; j++) {
            d = b / j;
            for (j = 1; j <= d; j++) {
                if (d % j == 0) {
                    y = j;
                    System.out.print(y + ", ");
                }
            }
        }
        System.out.println("\n");
        System.out.print("Jadi, faktor persekutuan dari " + a + " dan " + b + " adalah ");
        for (i = 1; i <= a; i++) {
            c = a / i;
            for (i = 1; i <= c; i++) {
                if (c % i == 0) {
                    x = i;
                    z = b / x;
                    v = b / z;
                    System.out.print(v + ", ");
                }
            }
        }
    }
}

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.DecimalFormat;

public class nomor4 {

    static double a1, b1, c1, h1;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String a = "", b = "", c = "", h = "";

        System.out.println();
        System.out.print("Persamaan kuadrat");

        String Input = input.nextLine();

        Pattern quadPattern = Pattern.compile("([+-]?[0-9]*)[Xx]\\^2([+-]?[0-9]*)[Xx]([+-]?[0-9]*)([=]+)([-]?[0-9]+)");
        Matcher quadMatcher = quadPattern.matcher(Input);
        input.close();

        // in case type format right
        if (quadMatcher.matches()) {
            Input = Input.replaceAll(" ", "");
            a = quadMatcher.group(1);
            b = quadMatcher.group(2);
            c = quadMatcher.group(3);
            h = quadMatcher.group(5);

            if (a.length() == 0) {
                a = "1";
            } else if (a.length() == 1 && (a.charAt(0) == '+') || a.charAt(0) == '-') {
                a += "1";
            }
            if (b.length() == 0) {
                b = "1";
            } else if (b.length() == 1 && (b.charAt(0) == '+') || b.charAt(0) == '-') {
                b += "1";
            }
            if (c.length() == 0) {
                c = "0";
            }

        } else {
            System.out.println("Format penulisan salah");
        }

        int A = 0, B = 0, C = 0, H = 0;
        double pers = 0, d = 0, x1 = 0, x2 = 0;

        A = Integer.parseInt(a);
        B = Integer.parseInt(b);
        C = Integer.parseInt(c);
        H = Integer.parseInt(h);
        C = C - H;

        pers = (B * B) - (4 * A * C);
        d = Math.sqrt(Math.abs(pers));

        if (pers > 0 || pers < 0) {
            x1 = (-B + d) / (2 * A);
            x2 = (-B - d) / (2 * A);
        } else if (pers == 0) {
            x1 = (-B) / (2 * A);
        } else {
            x1 = (-B + d) / (2 * A);
            x2 = Math.abs(d) / (2 * A);
        }
        DecimalFormat decimal = new DecimalFormat("#.#");
        String X1 = decimal.format(x1);
        String X2 = decimal.format(x2);

        BilanganPecahan root1 = new BilanganPecahan();
        BilanganPecahan root2 = new BilanganPecahan();

        if (X1.length() > 1) {
            String Akar1 = X1.replace(',', '.');
            double akar1 = Double.parseDouble(Akar1);

            root1.Akar1(akar1);

        } else if (X1.length() == 1) {
            double akar1 = Double.parseDouble(X1);

            root1.Akar1(akar1);

        }
        if (X2.length() > 1) {
            String Akar2 = X2.replace(',', '.');
            double akar2 = Double.parseDouble(Akar2);

            root2.Akar2(akar2);

        } else if (X2.length() == 1) {
            double akar2 = Double.parseDouble(X1);

            root2.Akar2(akar2);

        }
        String akar1 = root1.getX1();
        String akar2 = root2.getX2();

        root1.faktorx(akar1);
        root2.faktorx(akar2);
        int pilih = 0;
        System.out.println(root2.getFaktor() + root1.getFaktor() + " = 0");

        if (akar1.equals(akar2)) {
            System.out.println("x = " + akar1);

        } else {
            System.out.println("x = " + akar2 + " atau x = " + akar1);
        }

    }
}

class BilanganPecahan {
    public double akar1, akar2;
    public String x1, x2, formatFaktor;
    int ax1, bx1, ax2, bx2;

    public void Akar1(double akar1) {
        for (int i = 1;; i++) {
            double tem = akar1 / (1D / i);

            if (Math.abs(tem - Math.round(tem)) < 0.2) {
                if (i == 1) {
                    x1 = Math.round(tem) + "";

                } else {
                    x1 = Math.round(tem) + "/" + i;

                }
                break;
            }
        }
    }

    public void Akar2(double akar2) {
        for (int i = 1;; i++) {
            double tem = akar2 / (1D / i);

            if (Math.abs(tem - Math.round(tem)) < 0.2) {
                if (i == 1) {
                    x2 = "" + Math.round(tem);

                } else {
                    x2 = Math.round(tem) + "/" + i;

                }
                break;
            }
        }
    }

    public void faktorx(String akar1) {
        Pattern bentukFaktor = Pattern.compile("([+-]?[0-9]+)([/]*)([+-]?[0-9]*)");
        Matcher bentukAkar1 = bentukFaktor.matcher(akar1);

        if (bentukAkar1.matches()) {
            if (akar1.length() <= 2) {
                ax1 = Integer.parseInt(bentukAkar1.group(1));
                if (ax1 >= 0) {
                    formatFaktor = "(x-" + ax1 + ")";
                } else {
                    formatFaktor = "(x+" + (ax1 * -1) + ")";
                }

            } else {
                ax1 = Integer.parseInt(bentukAkar1.group(1));
                bx1 = Integer.parseInt(bentukAkar1.group(3));
                if (ax1 >= 0) {
                    formatFaktor = "(" + bx1 + "x-" + ax1 + ")";

                } else {
                    formatFaktor = "(" + bx1 + "x+" + (ax1 * -1) + ")";
                }

            }

        } else {
            formatFaktor = "Format salah!";
        }
    }

    public String getX1() {
        return x1;
    }

    public String getX2() {
        return x2;
    }

    public String getFaktor() {
        return formatFaktor;
    }
}
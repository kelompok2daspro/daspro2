package nomor5;
import java.util.Scanner;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class Nomor5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner java = new Scanner (System.in);
        int maks, min, a;
        
        System.out.println("++++--POSSIBLE NUMBER--++++");
        System.out.println(" ");
        System.out.print("String : ");
        String abc = java.nextLine();
        
        String m[] = new String[100];
        int n[] = new int[100];
        
        //mencari nilai maksimal untuk 2 digit 
        maks = n[0];
        a = 0;
        for (int i =0; i < abc.length(); i++){
            try {
                m [i] = abc.substring(i, i+2);
                n [i] = Integer.parseInt(m[i]);
            }catch (Exception e){
                continue;
            }
            if (maks <= n[i]){
                maks = n[i];
                a = i;
            }
        }
        
        //mencari nilai maksimal untuk 1 digit 
        if (maks == 0){
            for (int i = 0; i < abc.length(); i++ ) {
                try {
                    m[i] = abc.substring(i,(i+1));
                    n[i] = Integer.parseInt(m[i]);
                }catch(Exception e) {
                    continue;
                }
                if (maks <= n[i]){
                    maks = n[i];
                    a = i;
                }
            }
        }
        
        //mencari nilai minimal 
        min = n[0];
        for (int i = 0; i < abc.length(); i++) {
            try{
                m[i] = abc.substring(i,(i+1));
                n[i] = Integer.parseInt(m[i]);
            }catch (Exception e){
                continue;
            }
            
            
        //menghindari agar tidak ada angka yang sama 
         if ((i != a) && (i != a + 1)){
             if (min >= n[i]){
                 min = n[i];
             }
             
             System.out.println("Min : "+ min);
             System.out.println("Max : "+ maks);
             break;
         }else{
             System.out.println("Min : "+ "-");
             System.out.println("Max : "+ maks);
             break;
         }
            
        }
        
        //jika berisi huruf saja
        if (min ==0 && maks ==0){
            System.out.println("Min : "+ "-");
            System.out.println("Max : "+ "-");
        }
                
    }
    
}

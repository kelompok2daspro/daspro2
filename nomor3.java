import java.util.Scanner;

import java.lang.Math;

public class nomor3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Masukan char yang harus di decrypt");
        char[] str = s.next().toCharArray();

        int len = str.length;
        for (int i = 0; i < len; i++) {
            if (str[i] == '.')
                str[i] = '0';
            else
                str[i] = '1';
        }

        int i = len - 1, count = 0, prefixLength = 0;
        while (count < 8) {
            if (str[i] == '1') {
                prefixLength += Math.pow(2, count);
            }
            count++;
            i--;
        }

        int j = prefixLength, pangkat = 7, temp = 0;
        while (j <= i) {
            if (str[j] == '1') {
                temp += Math.pow(2, pangkat);
            }
            if (pangkat == 0) {
                System.out.print((char) temp);
                temp = 0;
                pangkat = 7;
            } else {
                pangkat--;
            }
            j++;
        }
        System.out.println();
    }
}
